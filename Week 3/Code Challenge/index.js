const express = require("express");
const app = express();
const helloRouter = require("./routes/helloRoute");

app.get("/:name", helloRouter);
app.post("/:name", helloRouter);
app.put("/:name", helloRouter);
app.delete("/:name", helloRouter);

app.listen(3000, () => console.log("Server Running on 3000!"));