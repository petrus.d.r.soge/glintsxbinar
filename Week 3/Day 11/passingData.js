const EventEmitter = require ("events");
const readline = require("readline");
const rl = readline.createInterface({
input: process.stdin,
output: process.stdout,
});
const my = new EventEmitter();

// make an event listener
my.on("Failed to login", (email, password) => {
    console.log(`Login Failed!`);
    rl.close();
});

my.on("Login Successful", (email) => {
    console.log(`${email} logged in!`);

    require ("../../Day 10/Challenge/challengeTwo");

    rl.close();
});

function login(email, password) {
    const passwordStoredInDatabase = "Linkin_Park";

    if (password != passwordStoredInDatabase) {
        my.emit("Failed to login", email, password);
    } else {
        my.emit("Login Successful", email);
    }
}

rl.question("Email: ", (email) => {
    rl.question("Password: ", (password) => {
        login(email, password);
    });        
});