const threeDimensional = require("./threeDimensional");

class Cone extends threeDimensional {
  constructor(radius, height, slantHeight) {
    super("Cone");
    this.radius = radius;
    this.height = height;
    this.slantHeight = slantHeight;
    
  }

  // Overloading method
  introduce(intro) {
    super.introduce();
    console.log(`${intro}, this is ${this.name} \n`);
  }

  // Overridding
  calculateVolume() {
    super.calculateVolume();
    let coneVolume = (1/3) * (Math.PI * this.radius ** 2) * this.height;

    console.log(`This cone is ${coneVolume} cm3. \n`);
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    let surfaceArea = Math.PI * this.radius * (this.radius + this.slantHeight);

    console.log(`This cone's surface area is ${surfaceArea} cm2. \n`);
  }
}

module.exports = Cone;
