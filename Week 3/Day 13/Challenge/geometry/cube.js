const threeDimensional = require("./threeDimensional");

class Cube extends threeDimensional {
  constructor(side) {
    super("Cube");
    this.side = side;
  }

  // Overloading method
  introduce(intro) {
    super.introduce();
    console.log(`${intro}, this is ${this.name} \n`);
  }

  // Overridding
  calculateVolume() {
    super.calculateVolume();
    let cubeVolume = this.side ** 3;

    console.log(`This cube volume is ${cubeVolume} cm3. \n`);
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    let surfaceArea = 12 * (this.side);

    console.log(`This cube's surface area is ${surfaceArea} cm. \n`);
  }
}

module.exports = Cube;
