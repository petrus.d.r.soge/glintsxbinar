const threeDimensional = require("./threeDimensional");

class Tube extends threeDimensional {
  constructor(radius, height) {
    super("Tube");
    this.radius = radius;
    this.height = height;
    
  }

  // Overloading method
  introduce(intro) {
    super.introduce();
    console.log(`${intro}, this is ${this.name} \n`);
  }

  // Overridding
  calculateVolume() {
    super.calculateVolume();
    let tubeVolume = Math.PI * this.radius ** 2 * this.height;

    console.log(`This tube volume is ${tubeVolume} cm3. \n`);
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    let surfaceArea = 2 * Math.PI * this.radius * (this.radius + this.height);

    console.log(`This tube surface area is ${surfaceArea} cm2. \n`);
  }
}

module.exports = Tube;
