const Geometry2D = require("./geometry2D");

class twoDimensional extends Geometry2D {
  constructor(name) {
    super(name, "2D");

   }

  calculateArea() {
    console.log(`${this.name} Area!`);
  }

  calculateCircumference() {
    console.log(`${this.name} Circumference!`);
  }
}

module.exports = twoDimensional;
