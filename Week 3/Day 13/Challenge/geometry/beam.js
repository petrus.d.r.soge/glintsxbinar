const threeDimensional = require("./threeDimensional");

class Beam extends threeDimensional {
  constructor(length, width, height) {
    super("Beam");
    this.length = length;
    this.width = width;
    this.height = height;
  }

  // Overloading method
  introduce(intro) {
    super.introduce();
    console.log(`${intro}, this is ${this.name} \n`);
  }

  // Overridding
  calculateVolume() {
    super.calculateVolume();
    let beamVolume = this.length * this.width * this.height;

    console.log(`This beam is ${beamVolume} cm3. \n`);
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    let surfaceArea = 4 * (this.length + this.width + this.height);

    console.log(`This beam's surface area is ${surfaceArea} cm2. \n`);
  }
}

module.exports = Beam;
