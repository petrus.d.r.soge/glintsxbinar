const Geometry3D = require("./geometry3D");

class threeDimensional extends Geometry3D {
  constructor(name) {
    super(name, "3D");

   }

    calculateVolume() {
    console.log(`${this.name} Volume!`);
  }

    calculateSurfaceArea() {
    console.log(`${this.name} Surface Area!`);
  }
}

module.exports = threeDimensional;
