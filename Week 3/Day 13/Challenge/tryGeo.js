const { Square, Rectangle, Triangle, Beam, Cube, Tube, Cone} = require("./geometry");

let trySquare = new Square(20);
trySquare.calculateArea();

let tryRectangle = new Rectangle(15, 27);
tryRectangle.calculateArea();

let tryTriangle = new Triangle(11, 15);
tryTriangle.calculateCircumference();

let tryBeam = new Beam(10, 10, 10);
tryBeam.calculateSurfaceArea();

let tryCube = new Cube(50);
tryCube.calculateSurfaceArea();

let tryTube = new Tube(50, 100);
tryTube.calculateVolume();

let tryCone = new Cone(5, 15, 35);
tryCone.calculateVolume();

