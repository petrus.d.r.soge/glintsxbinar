const Geometry = require("./geometry" );

class twoDimensional extends Geometry{
    constructor(name) {
        super(name, "2D");
    // Make abstract class
    if (this.constructor == twoDimensional) {
        throw new Error("Can not make object");
    }
}

    // Overriding method
    introduce() {
        super.introduce();
        console.log(`This is ${this.type}`);
    }

    calculateArea() {
        console.log(`${this.name} Area`);
    }

    calculateCircumference() {
        console.log(`${this.name} Circumference!`);
    }
}

module.exports = twoDimensional;