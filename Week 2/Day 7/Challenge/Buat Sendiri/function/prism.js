const index = require("../index"); // Import index to run rl on this file

// Function to calculate prism volume
function calculateVolumePrism(base, height, length) {
  return (1/2) * base * length * height;
}

// Function to input the length
function input() {
  index.rl.question("Base: ", (base) => {
    index.rl.question("Height: ", (height) => {
      index.rl.question("Length: ", (length) => {
        if (!isNaN(base) && !isNaN(height) && !isNaN(length)) {
          console.log(`\nPrism: ${calculateVolumePrism(base, length, height)}`);
          index.rl.close();
        } else {
          console.log("Input must be a number !\n");
          input();
        }
      });
    });
  });
}

module.exports = { input }; // Export the input, so the another file can run this code