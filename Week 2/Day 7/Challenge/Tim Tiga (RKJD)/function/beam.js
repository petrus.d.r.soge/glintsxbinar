const index = require("../index"); // Import index to run rl on this file

// Function to calculate cube volume
function calculateVolumeBeam(length, width, height) {
  return length * height * width;
}

// Function to input the length
function input() {
  index.rl.question("Length: ", (length) => {
    index.rl.question("Width: ", (width) => {
      index.rl.question("Height: ", (height) => {
        if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
          console.log(`\nBeam: ${calculateVolumeBeam(length, width, height)}`);
          index.rl.close();
        } else {
          console.log("Input must be a number !\n");
          input();
        }
      });
    });
  });
}

module.exports = { input }; // Export the input, so the another file can run this code
