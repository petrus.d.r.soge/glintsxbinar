const index = require("../index");
// function of cone
function cone(r, h) {
  let volume = (Math.PI * r ** 2 * h) / 3; //formula of volume
  return volume;
}


//input way 1
function input() {
  index.rl.question("radius: ", (radius) => {
    index.rl.question("height: ", (height) => {
      if (!isNaN(radius) && !isNaN(height)) {
        console.log("Cone's volume is "+cone(radius, height));
        index.rl.close();
      } else {
        console.log("radius and height must be number!");
        input();
      }
    });
  });
}
//

module.exports.input = input;