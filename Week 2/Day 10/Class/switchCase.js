let option = 6 ;

switch (option) {
    case 1:
    case 4:
    case 6:
        console.log("This is one!");
        break; // it will break here
    case 2:
        console.log("This is two!");
        break; // it will break here
    case 3:
        console.log("This is three!");
        break; // it will break here
    default:
        console.log("This is default!");
    // we don't need break because this is the last code
}