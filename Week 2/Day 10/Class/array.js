let fridge = ["Apple", "Milk", "Carrot", "Orange", "Leech", "Cabbage"];

    console.log(fridge.includes("Apple"));  // is fridge includes apple?
    console.log(fridge.includes("Banana")); // is fridge includes banana?

 // with manual case-insensitive
    for (var i = 0; i < fridge.length; i++){
      if (fridge[i].toLowerCase() == "apple"){
        console.log(true);
   }
  }