const express = require("express"); // Import express
const app = express(); // create express app

// import routes
const barangRoute = require ("./Routes/barangRoute");

//Use to read the req.body
app.use(express.urlencoded({ extended: true }));

//Define route
app.use("/barang", barangRoute);

// server run on port 3000
app.listen(5001, () => console.log("This server running on 5001!"));
