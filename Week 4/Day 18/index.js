const express = require("express"); // Import express
const app = express(); // create express app

// import routes
const transaksiRoute = require ("./Routes/transaksiRoute");

//Use to read the req.body
app.use(express.urlencoded({ extended: true }));

//Define route
app.use("/transaksi", transaksiRoute);

// server run on port 3000
app.listen(3000, () => console.log("This server running on 3000!"));
