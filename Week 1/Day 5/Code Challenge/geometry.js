// First volume calculate: Cone
function calculateConeVolume(r, h){
    let baseArea = (1/3)*Math.PI*r**2;
    let coneVolume = baseArea * h;
    
    return coneVolume;
    }
    
    let coneValue = calculateConeVolume(200, 600);
    let coneResult = coneValue;
    
    console.log ("Cone Volume = " + coneResult + " cm3");
    
    // Second volume calculate: Ellipsoid
    function calculateEllipsoidVolume(a, b, c){
    let axisArea = a*b*c;
    let ellipsoidVolume = (4/3)*Math.PI*axisArea;
    
    return ellipsoidVolume;
    }
    
    let ellipsoidValue = calculateEllipsoidVolume (500, 800, 900);
    let ellipsoidResult = ellipsoidValue;
    
    console.log("Ellipsoid Volume = " + ellipsoidResult + " cm3");