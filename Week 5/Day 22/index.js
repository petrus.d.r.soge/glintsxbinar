require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` }); // configure dotenv
const express = require("express"); // Import express

//Import router
const transaksiRoutes = require("./routes/transaksiRoutes");
// Make express app
const app = express();

// Body Parser
app.use(express.json()); // Enable json body
app.use(express.urlencoded({ extended: true })); // Enable body urlencode

// Make routes
app.use("/transaksi", transaksiRoutes);

// Run Server
app.listen(3000, () => console.log("Server running on 3000"));
