const express = require("express"); //Import express

// Import validator
const transaksiValidator = require("../middlewares/validators/transaksiValidator");

// Import Controller
const transaksiController = require("../controllers/transaksiController");

// Make router
const router = express.Router();

//make routes
router.get("/", transaksiController.getAll);
router.get("/:id", transaksiController.getOne);
router.post("/", transaksiValidator.create, transaksiController.create);
router.put("/:id", transaksiValidator.update, transaksiController.update);
router.delete("/:id", transaksiController.delete);

//Export router
module.exports = router;
