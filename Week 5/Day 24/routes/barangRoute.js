const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const barangValidator = require("../middlewares/validators/barangValidator");

// Import controller
const barangController = require("../controllers/barangController");

// If POST (/barang)
// Then, go to transaksiValidator.create
// If in the transaksiValidator.create can run the next(), it will go to transaksiController.create
router.post("/", barangValidator.create, barangController.create);

// Get all barang data
router.get("/", barangController.getAll);

// Get one barang
router.get("/:id", barangValidator.getOne, barangController.getOne);

// Update data barang
router.put("/:id,", barangValidator.update, barangController.update);

// Delete barang
router.delete("/:id", barangValidator.delete, barangController.delete);


module.exports = router; // Export router