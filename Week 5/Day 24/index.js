require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

// express
const express = require("express");
const fileUpload = require("express-fileupload");

//Import routes
const transaksiRoutes = require("./routes/transaksiRoutes");

const barangRoutes = require("./routes/barangRoute");
//Make express app
const app = express();

// Body-parser to read req.body
app.use(express.json()); //Enable req.body JSON type
app.use(
  express.urlencoded({
    extended: true,
  })
); // support urlencode body

// To read form-data request
app.use(fileUpload());

// Set static file directory
app.use(express.static("public"));

// Make routes
app.use("/transaksi", transaksiRoutes);
app.use("/barang", barangRoutes);

app.listen(3000, () => console.log("server running on 3000"));
