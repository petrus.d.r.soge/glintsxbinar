const mongoose = require("mongoose");
const validator = require("validator");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.create = async (req, res, next) => {
  // Initialita
  let errors = [];

  // Check id_pemasok
  if (!mongoose.Types.ObjectId.isValid(req.body.id_pemasok)) {
    errors.push(
      "id_pemasok is not valid and must be 24 character & hexadecimal"
    );
  }

  // If params error
  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  // find pemasok
  let dataPemasok = await pemasok.findOne({ _id: req.body.id_pemasok });

  // If data pemasok not found
  if (!dataPemasok) {
    errors.push("Pemasok not found");
  }

  // Check harga is number
  if (!validator.isNumeric(req.body.harga)) {
    errors.push("Harga must be a number");
  }

  // If errors length > 0, it will make errors message
  if (errors.length > 0) {
    // Because bad request
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  req.body.pemasok = req.body.id_pemasok;

  // It means that will be go to the next middleware
  next();
};
exports.update = async (req, res, next) => {
  let errors = [];

  // Check parameter id is valid or not
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    errors.push(
      "id_transaksi is not valid and must be 24 character & hexadecimal"
    );
  }
  // Check id_barang is valid or not
  if (!mongoose.Types.ObjectId.isValid(req.body.id_barang)) {
    errors.push(
      "id_barang is not valid and must be 24 character & hexadecimal"
    );
  }

  // Check id_pemasok is valid or not
  if (!mongoose.Types.ObjectId.isValid(req.body.id_pemasok)) {
    errors.push(
      "id_pelanggan is not valid and must be 24 character & hexadecimal"
    );
  }

  // If the parameters is not valid it will go here
  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  // Find barang, harga, jumlah, image
  let findData = await Promise.all([
    barang.findOne({ _id: req.body.id_barang }),
    pemasok.findOne({ _id: req.body.id_pemasok }),
    image.findOne({ _id: req.files.image }),
  ]);

  // if barang not found
  if (!findData[0]) {
    errors.push("Barang not found");
  }

  // If harga not found
  if (!findData[1]) {
    errors.push("Pemasok not found");
  }

  // If pemasok not found
  if (!findData[2]) {
    errors.push("Transaksi not found");
  }

  // If error
  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  // Go to next
  next();
};
exports.getOne = (req, res, next) => {
  // Check parameter is valid or not
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).json({
      message: "Parameter is not valid and must be 24 character & hexadecimal",
    });
  }

  next();
};
exports.delete = async (req, res, next) => {
  let errors = [];

  // Check params is valid or not
  if (!mongoose.Types.ObjectId.isValid(req.params.id_barang)) {
    errors.push(
      "id_barang is not valid and must be 24 characters & hexadecimal"
    );
  }

  // If params error
  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  // Find one transaksi
  let data = await transaksi.findOne({ _id: req.params.id });

  // If transaksi not found
  if (!data) {
    errors.push("Transaksi not found");
  }

  // If error
  if (errors.length > 0) {
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  // Go to next
  next();
};
