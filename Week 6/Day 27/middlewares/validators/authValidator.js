const validator = require("validator");

exports.signup = async (req, res, next) => {
  let errors = [];

  // Check req.body.email is email
  if (!validator.isEmail(req.body.email)) {
    errors.push("Email field must be a valid email");
  }

  // check password strong
  if (!validator.isStrongPassword(req.body.password)) {
    errors.push(
      "Password must have at least 8 characters, (minimum 1 lowercase letter, 1 uppercase letter, 1 number, 1 symbol)"
    );
  }
  // Check password confirmation
  if (req.body.confirmPassword !== req.body.password) {
    errors.push("Password confirmation must be same to password");
  }

  // if errors length > 0, it will make errors message
  if (errors.length > 0) {
    // Because of bad request
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  next();
};

exports.signin = async (req, res, next) => {
  let errors = [];

  // Check req.body.email is email
  if (!validator.isEmail(req.body.email)) {
    errors.push("Email field must be a valid email");
  }

  // check password strong
  if (!validator.isStrongPassword(req.body.password)) {
    errors.push(
      "Password must have at least 8 characters, (minimum 1 lowercase letter, 1 uppercase letter, 1 number, 1 symbol)"
    );
  }

  // if errors length > 0, it will make errors message
  if (errors.length > 0) {
    // Because of bad request
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  next();
};
