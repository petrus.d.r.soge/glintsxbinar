const request = require("supertest"); // Import supertest
const app = require("../index"); // import server
const { user, transaksi } = require("../models"); // import user
let authenticationToken; // Variable to save token

const deleteAllData = async () => {
  await user.deleteMany();
};

deleteAllData();

describe("User Test", () => {
  describe("/auth/signup POST", () => {
    it("It should make user and get the token", async () => {
      const res = await request(app).post("/auth/signup").send({
        email: "dedi@gmail.com",
        password: "Dh3koz!?",
        confirmPassword: "Dh3koz!?",
        name: "Dedie",
      });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("token");

    //   authenticationToken = res.body.token;
    });
  });
});
