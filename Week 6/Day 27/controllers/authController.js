const jwt = require("jsonwebtoken"); // import jasonwebtoken

class AuthController {
  async getToken(req, res) {
    try {
      // Get the req.user from passport auth that has been created in the authRouthes
      //and create body variable
      const body = {
        user: {
          id: req.user._id,
        },
      };

      // Create jwt token with { user : {id: req.user._id }} value
      //and the key is process.env.JwT_SECRET
      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "60d",
      });

      // If Success
      return res.status(200).json({
        message: "Success",
        token,
      });
    } catch (e) {
      //if error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}
module.exports = new AuthController();
